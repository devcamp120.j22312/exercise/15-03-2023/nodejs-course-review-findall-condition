const getAllCourseMiddleware = (req, res, next) => {
    console.log("Get All Course");
    next();
}

const getCourseMiddleware = (req, res, next) => {
    console.log("Get Course");
    next();
}

const postCourseMiddleware = (req, res, next) => {
    console.log("Create a Course");
    next();
}

const putCourseMiddleware = (req, res, next) => {
    console.log("Update a Course");
    next();
}

const deleteCourseMiddleware = (req, res, next) => {
    console.log("Delete Course");
    next();
}

module.exports = {
    getAllCourseMiddleware,
    getCourseMiddleware,
    postCourseMiddleware,
    putCourseMiddleware,
    deleteCourseMiddleware
}