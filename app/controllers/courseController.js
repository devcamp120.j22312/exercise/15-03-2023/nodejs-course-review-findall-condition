const mongoose = require('mongoose');

// B1: Import Model
// Import course Model
const courseModel = require('../models/courseModel')

// B2: Tạo function CRUD
// Get all course
const getAllCourse = (request, response) => {
    // B1: Thu thập dữ liệu
    let courseName = request.query.courseName;
    let minStudent = request.query.minStudent;
    let maxStudent = request.query.maxStudent;

    // Tạo ra điều kiện lọc
    let condition = {};
    if (courseName) {
        condition.title = courseName;
    }
    if(minStudent) {
        condition.noStudent = { $gte : minStudent}
    }
    if(maxStudent) {
        condition.noStudent = { ...condition.noStudent, $lte : maxStudent}
    }

    let condition2 = {
        title: courseName,
        $or : [ 
            { noStudent: { $lte : minStudent }},
            { noStudent: { $gte : maxStudent }}
        ]
    }

    // B2: Kiểm tra
    // B3: Xử lý và trả về kết quả 
    courseModel.find(condition, {_id: 0, title: 1, description: 1}, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully load all data!',
                course: data
            })
        }
    })
}
// Get a course by id
const getCourseById = (request, response) => {
    // B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    // B2: Kiểm tra
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            message: "CourseId is invalid!"
        })
    }

    // B3: Xử lý và trả về kết quả 
    courseModel.findById(id, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully get course data!',
                course: data
            })
        }
    })
}

// Create new course
const createCourse = (request, response) => {
    // B1: Thu thập dữ liệu
    let body = request.body;

    // B2: Kiểm tra dữ liệu
    if (!body.title) {
        return response.status(400).json({
            message: 'title is required!'
        })
    }

    if (!Number.isInteger(body.noStudent)) {
        return response.status(400).json({
            message: 'noStudent is invalid!'
        })
    }
    // B3: Xử lý và trả về kết quả
    let newCourse = new courseModel({
        _id: mongoose.Types.ObjectId(),
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    });
    courseModel.create(newCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully created!',
                course: data
            })
        }
    })
}

// Update course by id
const updateCourseById = (request, response) => {
    // B1: Thu thập dữ liệu
    let courseId = request.params.courseId;
    let body = request.body;

    // B2: Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            message: "CourseId is invalid!"
        })
    }
    if (!body.title) {
        return response.status(400).json({
            message: 'title is required!'
        })
    }
    if (Number.isInteger(body.noStudent) || body.noStudent < 0) {
        return response.status(400).json({
            message: 'noStudent is invalid!'
        })
    }
    // B3: Xử lý và trả về kết quả
    let updateCourse = {
        title: body.title,
        description: body.description,
        noStudent: body.noStudent
    };

    courseModel.findByIdAndUpdate(courseId, updateCourse, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(201).json({
                message: 'Successfully updated!',
                course: data
            })
        }
    })
}

// Delete course by id
const deleteCourseById = (request, response) => {
    // B1: Thu thập dữ liệu
    let courseId = request.params.courseId;

    // B2: Kiểm tra dữ liệu
    if (mongoose.Types.ObjectId.isValid(courseId)) {
        return response.status(400).json({
            message: "CourseId is invalid!"
        })
    }

    // B3: Xử lý và trả về kết quả
    courseModel.findByIdAndDelete(courseId, (error, data) => {
        if (error) {
            return response.status(500).json({
                message: `Internal server error: ${error.message}`
            });
        } else {
            return response.status(200).json({
                message: 'Successfully deleted!',
                course: data
            })
        }
    })
}

// B3: Export thành module
module.exports = {
    getAllCourse,
    getCourseById,
    createCourse,
    updateCourseById,
    deleteCourseById
}