// Khai báo thư viện express
const express = require('express');

// Import controller
const { 
    getAllCourse,
    getCourseById,
    createCourse,
    updateCourseById,
    deleteCourseById
        } = require('../controllers/courseController');

// Khai báo middleware
const {
    getAllCourseMiddleware,
    getCourseMiddleware,
    postCourseMiddleware,
    putCourseMiddleware,
    deleteCourseMiddleware
        } = require('../middlewares/courseMiddleware');

// Tạo ra router
const router = express.Router();

router.get('/courses', getAllCourseMiddleware, getAllCourse)

router.get('/courses/:courseId', getCourseMiddleware, getCourseById)

router.post('/courses', postCourseMiddleware, createCourse)

router.put('/courses/:courseId', putCourseMiddleware, updateCourseById)

router.delete('/courses/:courseId', deleteCourseMiddleware, deleteCourseById)

module.exports = router;