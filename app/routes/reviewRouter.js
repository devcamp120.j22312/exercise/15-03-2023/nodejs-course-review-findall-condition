// Import bộ thư viện express
const express = require('express'); 

const { createReviewOfCourse, getAllReviewOfCourse, getReviewById, updateReviewById, deleteReviewById } = require("../controllers/reviewController");

const router = express.Router();

router.post("/courses/:courseId/reviews", createReviewOfCourse);

router.get("/courses/:courseId/reviews", getAllReviewOfCourse);

router.get("/reviews/:reviewId", getReviewById);

router.put("/reviews/:reviewId", updateReviewById);

router.delete("/courses/:courseId/reviews/:reviewId", deleteReviewById);

// Export dữ liệu thành 1 module
module.exports = router;
