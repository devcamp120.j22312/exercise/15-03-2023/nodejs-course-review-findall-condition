// Khai báo thư viện express
const express = require('express');
const courseRouter = require('./app/routes/courseRouter');
const reviewRouter = require('./app/routes/reviewRouter');

// Khai báo app
const app = express();

// Khai báo port
const port = 8000;

// Import thư viện mongoose
const mongoose = require('mongoose');

// Gọi review Model
const reviewModel = require("./app/models/reviewModel");
const courseModel = require("./app/models/courseModel");

// Cấu hình request đọc được body json
app.use(express.json());

app.use((request, response, next) => {
    console.log("Time", new Date());
    next();
},
(request, response, next) => {
    console.log("Request method: ", request.method);
    next();
}
)

// Kết nối với MongoDB
mongoose.connect("mongodb://127.0.0.1:27017/CourseDatabase", (err) => {
    if (err) throw err;

    console.log("Connect MongoDB Successfully!");
})

app.use('/', courseRouter)
app.use('/', reviewRouter)

// Khởi động app
app.listen(port, () => {
    console.log("App listening on port: ", port);
})